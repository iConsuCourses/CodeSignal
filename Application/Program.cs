﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Application
{
    class Algoritmos
    {
        //Regresa el siglo al que pertenece el año del input.
        public static int enturyFromYear(int year) => year = (year % 100 == 0) ? (year / 100) : ((year / 100) + 1);

        //Crear un palindromo (palabra que se lee de la misma forma de izquierda a derecha y viceversa).
        public static bool checkPalindrome(string inputString)
        {
            char[] array = inputString.ToCharArray();
            Array.Reverse(array);
            return new string(array).Equals(inputString);
        }

        public static int adjacentElementsProduct(int[] inputArray)
        {
            int[] array = new int[(inputArray.Length - 1)];
            //-23, 4, -3, 8, -12
            for (int i = 0; i < (inputArray.Length - 1); i++)
            {
                array[i] = inputArray[i] * inputArray[(i + 1)];
                Console.WriteLine(array[i]);
            }
            return array.Max();
        }

        public static int shapeArea(int n) => n * n + (n - 1) * (n - 1);

        public static int makeArrayConsecutive2(int[] statues)
        {
            int x = 0;
            Array.Sort(statues);
            for (int i = 0; i < (statues.Length - 1); i++)
                if (statues[(i + 1)] - statues[i] != 1)
                    x += statues[(i + 1)] - statues[i] - 1;
            return x;
        }

        public static bool almostIncreasingSequence(int[] sequence)
        {
            bool foundOne = false;
            for (int i = -1, j = 0, k = 1; k < sequence.Length; k++)
            {
                bool deleteCurrent = false;
                if (sequence[j] >= sequence[k])
                {
                    if (foundOne)
                        return false;
                    foundOne = true;
                    if (k > 1 && sequence[i] >= sequence[k])
                        deleteCurrent = true;
                }
                if (!foundOne)
                    i = j;
                if (!deleteCurrent)
                    j = k;
            }
            return true;
        }

        public static int matrixElementsSum(int[][] matrix)
        {
            int sum = 0;
            for (int i = 0; i < matrix.Length; i++)
                for (int j = 0; j < matrix[i].Length; j++)
                    if (matrix[i][j].Equals(null) || matrix[i][j].Equals(0))
                        for (int a = i; a < matrix.Length; a++)
                            matrix[a][j] = 0;
                    else
                        sum += matrix[i][j];
            return sum;
        }

        public static string[] allLongestStrings(string[] inputArray)
        {
            Array.Sort(inputArray, (x, y) => y.Length.CompareTo(x.Length));
            List<string> list = new List<string>();
            list.InsertRange(0, inputArray);
            for (int i = 0; i < (list.Count - 1); i++)
                if (list[i].Length > list[(i + 1)].Length)
                    list.RemoveRange((i + 1), (list.Count - (i + 1)));
            return list.ToArray();
        }

        public static int commonCharacterCount(string s1, string s2)
        {
            //s1 = aabcc
            //s2 = adcaa
            int sum = 0;
            for (int i = 0; i < s2.Length; i++)
            {
                if (Regex.IsMatch(s1, s2[i].ToString()))
                {
                    sum++;
                    //s1 = s1.Remove(i, 1);
                }
            }
            return sum;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Menú: ");
            byte x = byte.Parse(Console.ReadLine());
            switch (x)
            {
                case 1:
                    Console.Write("Año: ");
                    int year = int.Parse(Console.ReadLine());
                    Console.WriteLine(Algoritmos.enturyFromYear(year));
                    break;
                case 2:
                    Console.Write("Palabra: ");
                    string palindrome = Console.ReadLine();
                    Console.WriteLine(Algoritmos.checkPalindrome(palindrome));
                    break;
                case 3:
                    int[] arreglo = new int[] { -23, 4, -3, 8, -12 };
                    Console.WriteLine(Algoritmos.adjacentElementsProduct(arreglo));
                    break;
                case 4:
                    Console.WriteLine("Número: ");
                    int n = int.Parse(Console.ReadLine());
                    Console.WriteLine(Algoritmos.shapeArea(n));
                    break;
                case 5:
                    Console.Write("Número: ");
                    n = int.Parse(Console.ReadLine());
                    Console.WriteLine(Algoritmos.shapeArea(n));
                    break;
                case 6:
                    arreglo = new int[] { 2, 4, -1 };
                    Console.WriteLine(Algoritmos.makeArrayConsecutive2(arreglo));
                    break;
                case 7:
                    arreglo = new int[] { 40, 50, 60, 10, 20, 30 };
                    Console.WriteLine(Algoritmos.almostIncreasingSequence(arreglo));
                    break;
                case 8:
                    int[][] matriz = new int[][] { new int[] { 0, 1, 1, 2 }, new int[] { 0, 5, 0, 0 }, new int[] { 2, 0, 3, 3 } };
                    Console.WriteLine(Algoritmos.matrixElementsSum(matriz));
                    break;
                case 9:
                    string[] inputArray = new string[] { "aba", "aa", "ad", "vcd", "aba" };
                    Console.WriteLine(Algoritmos.allLongestStrings(inputArray));
                    break;
                case 10:
                    palindrome = "aabcc";
                    string s2 = "adcaa";
                    Console.WriteLine(Algoritmos.commonCharacterCount(palindrome, s2));
                    break;
            }
            Console.ReadKey();
        }
    }
}
